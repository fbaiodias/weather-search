import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  background: white;
  min-width: 140px;
  min-height: 140px;
  margin-bottom: 30px;
  padding: 10px;
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
`

const Name = styled.div`
  text-align: center;
  padding: 10px;
  font-size: 20px;
`
const Weather = styled.div`
  text-align: center;
  font-size: 16px;
  flex: 1;
`

const Row = styled.div`
  display: flex;
  line-height: 20px;
`
const Label = styled.label`
  font-size: 10px;
  min-width: 60px;
`
const Value = styled.label`
  margin-left: 10px;
`

export default ({name, temperature, humidity, weather}) => (
  <Wrapper>
    <Name>{name}</Name>
    <Weather>{weather}</Weather>
    <Row>
      <Label>Temperature</Label>
      <Value>{temperature.toFixed()}º</Value>
    </Row>
    <Row>
      <Label>Humidity</Label>
      <Value>{humidity}%</Value>
    </Row>
</Wrapper>
)
