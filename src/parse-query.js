import {
  RAINY,
  SUNNY,
  WARM,
  COLD,
  DRY,
  HUMID,
  CLOUDY
} from './constants'

export default search => {
  const filter = []

  if (search.indexOf('sunny') !== -1 || search.indexOf('clear') !== -1) {
    filter.push(SUNNY)
  }
  if (search.indexOf('rain') !== -1) {
    filter.push(RAINY)
  }
  if (search.indexOf('dry') !== -1) {
    filter.push(DRY)
  }
  if (search.indexOf('warm') !== -1 || search.indexOf('hot') !== -1) {
    filter.push(WARM)
  }
  if (search.indexOf('humid') !== -1 || search.indexOf('wet') !== -1) {
    filter.push(HUMID)
  }
  if (search.indexOf('cold') !== -1) {
    filter.push(COLD)
  }
  if (search.indexOf('cloud') !== -1) {
    filter.push(CLOUDY)
  }

  return filter
}
