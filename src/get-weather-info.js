import {
  RAINY,
  SUNNY,
  WARM,
  COLD,
  DRY,
  HUMID,
  CLOUDY
} from './constants'

const CITIES = [
  '524901',
  '703448',
  '2643743',
  '3128760', // Barcelona
  '2267057', // Lisbon
  '2988507', // Paris
  '4219762', // Rome
  '2759794', // Amsterdam
]

export default () =>
  fetch(`http://api.openweathermap.org/data/2.5/group?id=${CITIES.join(',')}&units=metric&appid=${process.env.REACT_APP_API_KEY}`)
    .then(res => res.json())
    .then(results => {
      console.log(results)
      return results.list.map(item => {
        const weather = item.weather[0].main
        const temperature = item.main.temp
        const humidity = item.main.humidity
        const tags = []
        if (temperature > 18) {
          tags.push(WARM)
        } else {
          tags.push(COLD)
        }
        if (humidity > 60) {
          tags.push(HUMID)
        } else {
          tags.push(DRY)
        }
        if (weather === 'Clear') {
          tags.push(SUNNY)
        }
        if (weather === 'Clouds') {
          tags.push(CLOUDY)
        }
        if (weather === 'Rain') {
          tags.push(RAINY)
        }

        return {
          name: item.name,
          weather,
          tags,
          temperature,
          humidity
        }
      })
    })