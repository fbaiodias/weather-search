import React, { Component } from 'react'
import styled from 'styled-components'
import getWeatherInfo from './get-weather-info'
import parseQuery from './parse-query'
import City from './city'

const Wrapper = styled.div`
  height: 100vh;
  width: 100vw;
`

const Header = styled.div`
  padding: 30px;
  border-bottom: 1px solid white;
`

const Input = styled.input`
  line-height: 50px;
  font-size: 30px;
  width: 100%;
  border: none;
  border-radius: 2px;
`

const Content = styled.div`
  padding: 30px;
  display: flex;
  justify-content: space-around;
  max-width: 100%;
  flex-wrap: wrap;
  flex: 1;
`

class App extends Component {
  constructor (props) {
    super(props)

    this.state = {
      search: '',
      results: []
    }

    this.handleChange = this.handleChange.bind(this)

    getWeatherInfo()
      .then(info => {
        console.log(info)
        this.setState({info, results: info})
      })
  }

  handleChange (e) {
    const search = e.target.value.toLowerCase()
    const filter = parseQuery(search)

    const results = this.state.info
      .filter(item => {
        return filter.filter(i => item.tags.indexOf(i) === -1).length === 0
      })

    this.setState({
      search,
      results
    })
  }

  render() {
    const {search, results} = this.state
    return (
      <Wrapper>
        <Header>
          <Input value={search} onChange={this.handleChange} />
        </Header>
        <Content>
          {results.map(item => (
            <City key={item.name} {...item} />
          ))}
        </Content>
      </Wrapper>
    );
  }
}

export default App;
