## weather-search

Live at http://weather-search.surge.sh/
(Best viewed on a mobile phone screen)

## Run

Set `REACT_APP_API_KEY` with a key from https://home.openweathermap.org/api_keys and run:
```bash
REACT_APP_API_KEY=<openweathermap api key> npm start
```
